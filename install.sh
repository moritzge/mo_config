#!/bin/bash

linkfile () {

	folder=$1
	filename=$2
	dirdest=$3


	filesource=$DIR/$folder/$filename
	filedest=$dirdest/$filename

	echo "Linking file '" $filename "': " $filesource " -> " $filedest
	
	mkdir -p $dirdest
	[ -e $filedest ] && rm -r $filedest
	ln -s $filesource $filedest
	
}

# path of script, no matter where it is called from
DIR="$( cd -P "$( dirname "$0" )" && pwd )"

# dracula theme for qt creator
linkfile qtcreator dracula.creatortheme ~/.config/QtProject/qtcreator/themes
linkfile qtcreator dracula.xml ~/.config/QtProject/qtcreator/styles
linkfile homeDirectory .Xmodmap  ~
linkfile homeDirectory .config/Typora/themes ~
