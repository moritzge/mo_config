./autogen.sh \
--enable-parallel \
--enable-chrome \
--enable-telegram \
--with-selection_color=#7E57C2 \
--with-second_selection_color=#9575CD \
--with-accent_color=#9575CD \
--with-suggestion_color=#673AB7 