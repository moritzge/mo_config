#!/bin/bash

# basics
sudo apt update
sudo apt install -y gnustep-gui-runtime # gopen

# sublime text
sudo apt update
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text sublime-merge
# do manually: 
# - install package control
gopen https://mail.google.com/mail/u/0/#search/sublime+text/FMfcgxwChcrgrwWTlwQBvTChvhLlczdK
gopen https://mail.google.com/mail/u/0/#search/sublime+text/FMfcgxvzKknhdsDKPnmxzJcDXrdqvDnD
# qtcreator
# gopen https://www.qt.io/offline-installers
# or:
sudo apt install -y qtcreator

# mo config
~/cd Documents/
git clone https://gitlab.com/moritzge/mo_config
cd mo_config/
ll
./install.sh
cd

# recursive font
gopen https://github.com/arrowtype/recursive/releases

## old theme
# zafiro icons
# gopen https://github.com/zayronxio/Zafiro-icons/releases/latest
# nordic theme
# gopen https://github.com/EliverLara/Nordic/releases/latest

## new theme
# blyr: https://github.com/yozoon/gnome-shell-extension-blyr
gopen https://extensions.gnome.org/extension/1251/blyr/
gopen https://extensions.gnome.org/extension/1011/dynamic-panel-transparency/
# Qogir theme
gopen https://github.com/vinceliuice/Qogir-theme
gopen https://github.com/vinceliuice/Qogir-icon-theme

# gnome tweak tool
sudo apt install -y gnome-tweak-tool

# gnome terminal dracula
sudo apt-get install dconf-cli
cd ~/Downloads/
git clone https://github.com/dracula/gnome-terminal
cd gnome-terminal/
./install.sh
cd

# gnome extensions
gopen https://extensions.gnome.org/extension/19/user-themes/
gopen https://extensions.gnome.org/extension/690/easyscreencast/
gopen https://extensions.gnome.org/extension/779/clipboard-indicator/

# ssh for gitlab
ssh-keygen 
cat ~/.ssh/id_rsa.pub
gopen https://gitlab.inf.ethz.ch/profile/keys

# dropbox 
gopen https://www.dropbox.com/download?dl=packages/ubuntu/dropbox_2020.03.04_amd64.deb

# seafile
# gopen https://www.seafile.com/en/download/
# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8756C4F765C9AC3CB6B85D62379CE192D401AB61
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8756C4F765C9AC3CB6B85D62379CE192D401AB61
echo deb http://deb.seadrive.org disco main | sudo tee /etc/apt/sources.list.d/seafile.list
sudo apt-get install seafile-gui

# dev tools
sudo apt install -y git cmake g++ xorg-dev libglu1-mesa-dev

# typora
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
sudo add-apt-repository 'deb https://typora.io/linux ./'
sudo apt-get update
sudo apt-get install typora

# git lg
git config --global alias.lg "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(normal)%s%C(reset) %C(dim normal)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
# git bash prompt
# add this to ~/.bashrc:
# PS1='\[\033[0;32m\]\[\033[0m\033[01;32m\]\u\[\033[01;36m\] @\h \[\033[01;34m\]\w\[\033[01;31m\]$(__git_ps1)\n\[\033[0;32m\]└─\[\033[0m\033[0;32m\] \$\[\033[0m\033[0;32m\] ▶\[\033[0m\] '